const express = require ('express')
const ItemController = require('../controllers/itemController')
const router = express.Router()


router.get("/item", ItemController.findAll)
router.get("/item/:id", ItemController.findOne)
router.post("/item", ItemController.create)
router.delete("/item/:id", ItemController.destroy)

module.exports = router