const express = require ('express')
const router = express.Router()
const itemRouter = require('./itemRoutes')

router.use(itemRouter)

module.exports = router