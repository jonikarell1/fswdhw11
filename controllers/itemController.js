const {Item} = require('../models')

class ItemController {

    static findAll = async (req, res, next) =>{

        try {
            const result = await Item.findAll({
                where: {
                    status: "active"
                }
            })

            if(result){
                res.status(200).json({
                    data: result
                })
            } else {
                throw ({name: 'ErrorNotFound'})
            }
            
        } catch (err) {
            next(err)
        } 
    }

    static findOne = async (req, res, next) =>{

            try {
                const {id} = req.params;
                const result = await Item.findOne({
                            where:{
                                id: id
                            }
                })

                if (result){
                    res.status(200).json({
                        data: result
                    })
                }else {
                    throw ({name: 'ErrorNotFound'})
                }                
            } catch (err) {
                next(err)
            }
            

           

    }

    static create = async (req, res, next)=>{

        try {
            const {name, status, type} = req.body
            const result = await Item.create({
                name: name,
                status: status,
                type: type
            })
          
             res.status(200).json({
                message: "ItemCreated Successfuly!"
            });
          
            
        } catch (err) {
            next(err)
        }
    }

    static destroy = async (req, res, next)=>{

        try {
            const {id} = req.params

            const result = await Item.update({
                status: "inactive"
            },{
                where: {
                    id
                }
            })
            
            if(result[0]=== 1){
                 res.status(200).json({
                        message: "Delete Successfully"
                    })
            } else{
                throw ({name: 'ErrorNotFound'})
            }
        } catch (err) {
            next(err)
        }
    }
    

}
       

module.exports = ItemController