const app = require('../app')
const request = require('supertest');
const { response } = require('../app');

describe('API /items', () => {

    it("test get /items", (done) => {
        request(app)
        .get('/item')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
           const firstData = response.body.data[0]
           expect(firstData.id).toBe(1)
            done()
        })
        .catch(done)

    })

     it("test get /items/:id", (done) => {
        request(app)
        .get('/item/2')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            const data = response.body.data
            expect(data.id).toBe(2)
            done()
        })
        .catch(done)

    })

    it('test post /item', (done) => {
        request(app)
        .post('/item')
        .send({name: 'kursi', status: 'active', type: 'furniture'})
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            const data = response.body
            expect(data.message).toBe('ItemCreated Successfuly!')
            done()      
        })
        .catch(done)
    });
    
    it("test delete /items/:id", (done) => {
        request(app)
        .delete('/item/11')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            const data = response.body
            expect(data.message).toBe('Delete Successfully')
            done()
        })
        .catch(done)

    })
})